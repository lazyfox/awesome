#!/bin/bash

CONF_FILE=$HOME/.asoundrc
if [ -f $CONF_FILE ]
then
    rm $CONF_FILE
fi

#Переключение мониторов
if (xrandr | grep "HDMI1 disconnected"); then
    xrandr --output HDMI1 --off --output LVDS1 --auto
else
    xrandr --output LVDS1 --mode 1600x900 --pos 1360x0 \
    --output HDMI1  --mode 1360x768 --pos 0x0
    echo "defaults.pcm.card 0" > $CONF_FILE
    echo "defaults.pcm.device 3" >> $CONF_FILE
    echo "defaults.ctl.card 0" >> $CONF_FILE
fi

#Исправление обоев
#feh --bg-fill ~/Pictures/wallpapers/girl_03.jpg
#$HOME/.i3/wallpaper.sh

#Перемещение коньков
#killall conky
#i3 restart
#$HOME/.i3/conky-desktop.sh
