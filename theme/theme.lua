-------------------------------
--      My awesome theme     --
--                           --
-------------------------------

-- Alternative icon sets and widget icons:
--  * http://awesome.naquadah.org/wiki/Nice_Icons

-- {{{ Main
theme = {}
theme.wallpaper = home_dir .. ".config/awesome/theme/3.jpg"
-- }}}

-- {{{ Styles
--theme.font      = "Sans Bold 8"
theme.font      = "FontAwesome Bold 9"

-- {{{ Colors
theme.fg_normal  = "#806157"
theme.fg_focus   = "#3b7c87"
theme.fg_urgent  = "#CC9393"
theme.bg_normal  = "#191311"
theme.bg_focus   = "#191311"
theme.bg_urgent  = "#191311"
theme.bg_systray = theme.bg_normal
-- }}}

-- {{{ Borders
theme.border_width  = 2
theme.border_normal = "#45342e"
theme.border_focus  = "#45342e"
theme.border_marked = "#45342e"
-- }}}

-- {{{ Titlebars
theme.titlebar_bg_focus  = "#3F3F3F"
theme.titlebar_bg_normal = "#3F3F3F"
-- }}}

-- There are other variable sets
-- overriding the default one when
-- defined, the sets are:
-- [taglist|tasklist]_[bg|fg]_[focus|urgent]
-- titlebar_[normal|focus]
-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- Example:
-- theme.taglist_bg_focus = "#CC9393"
theme.taglist_font = "FontAwesome 11"
-- }}}

-- {{{ Widgets
-- You can add as many variables as
-- you wish and access them by using
-- beautiful.variable in your rc.lua
--theme.fg_widget        = "#AECF96"
--theme.fg_center_widget = "#88A175"
--theme.fg_end_widget    = "#FF5656"
--theme.bg_widget        = "#494B4F"
--theme.border_widget    = "#3F3F3F"
-- }}}

-- {{{ Mouse finder
theme.mouse_finder_color = "#CC9393"
-- mouse_finder_[timeout|animate_timeout|radius|factor]
-- }}}

-- {{{ Menu
-- Variables set for theming the menu:
-- menu_[bg|fg]_[normal|focus]
-- menu_[border_color|border_width]
theme.menu_height = 20
theme.menu_width  = 150
-- }}}

-- {{{ Icons
-- {{{ Applications
theme.firefox_icon =    icons_dir .. "firefox.png"
theme.vim_icon =        icons_dir .. "vim-2.png"
theme.file_icon =       icons_dir .. "system-file-manager-6.png"
theme.browser_icon =    icons_dir .. "internet-web-browser.png"
theme.settings_icon =   icons_dir .. "system-settings-2.png"
theme.terminal_icon =   icons_dir .. "utilities-terminal-6.png"
theme.editor_icon =     icons_dir .. "accessories-text-editor-2.png"
-- }}}
-- {{{ Widgets
theme.widget_mem =      icons_dir .. "mem.png"
theme.widget_cpu =      icons_dir .. "cpu.png"
theme.widget_batt =     icons_dir .. "bat.png"
-- }}}


-- {{{ Taglist
theme.taglist_squares_sel   = theme_dir .. "taglist/squarefz.png"
theme.taglist_squares_unsel = theme_dir .. "taglist/squarez.png"
--theme.taglist_squares_resize = "false"
-- }}}

-- {{{ Misc
theme.awesome_icon           = icons_dir .. "distributions-gentoo.png"
theme.menu_submenu_icon      = "/usr/share/awesome/themes/default/submenu.png"
-- }}}

-- {{{ Layout
theme.layout_tile       = theme_dir .. "layouts/tile.png"
theme.layout_tileleft   = theme_dir .. "layouts/tileleft.png"
theme.layout_tilebottom = theme_dir .. "layouts/tilebottom.png"
theme.layout_tiletop    = theme_dir .. "layouts/tiletop.png"
theme.layout_fairv      = theme_dir .. "layouts/fairv.png"
theme.layout_fairh      = theme_dir .. "layouts/fairh.png"
theme.layout_spiral     = theme_dir .. "layouts/spiral.png"
theme.layout_dwindle    = theme_dir .. "layouts/dwindle.png"
theme.layout_max        = theme_dir .. "layouts/max.png"
theme.layout_fullscreen = theme_dir .. "layouts/fullscreen.png"
theme.layout_magnifier  = theme_dir .. "layouts/magnifier.png"
theme.layout_floating   = theme_dir .. "layouts/floating.png"
-- }}}

-- {{{ Titlebar
local titlebar_dir = theme_dir .. "titlebar/"
theme.titlebar_close_button_focus  = titlebar_dir .. "close_focus.png"
theme.titlebar_close_button_normal = titlebar_dir .. "close_normal.png"

theme.titlebar_ontop_button_focus_active  = 
    titlebar_dir .. "ontop_focus_active.png"
theme.titlebar_ontop_button_normal_active = 
    titlebar_dir .. "ontop_normal_active.png"
theme.titlebar_ontop_button_focus_inactive  = 
    titlebar_dir .. "ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_inactive = 
    titlebar_dir .. "ontop_normal_inactive.png"

theme.titlebar_sticky_button_focus_active  = 
    titlebar_dir .. "sticky_focus_active.png"
theme.titlebar_sticky_button_normal_active = 
    titlebar_dir .. "sticky_normal_active.png"
theme.titlebar_sticky_button_focus_inactive  = 
    titlebar_dir .. "sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_inactive = 
    titlebar_dir .. "sticky_normal_inactive.png"

theme.titlebar_floating_button_focus_active  = 
    titlebar_dir .. "floating_focus_active.png"
theme.titlebar_floating_button_normal_active = 
    titlebar_dir .. "floating_normal_active.png"
theme.titlebar_floating_button_focus_inactive  = 
    titlebar_dir .. "floating_focus_inactive.png"
theme.titlebar_floating_button_normal_inactive = 
    titlebar_dir .. "floating_normal_inactive.png"

theme.titlebar_maximized_button_focus_active  = 
    titlebar_dir .. "maximized_focus_active.png"
theme.titlebar_maximized_button_normal_active = 
    titlebar_dir .. "maximized_normal_active.png"
theme.titlebar_maximized_button_focus_inactive  = 
    titlebar_dir .. "maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_inactive = 
    titlebar_dir .. "maximized_normal_inactive.png"
-- }}}
-- }}}

return theme
